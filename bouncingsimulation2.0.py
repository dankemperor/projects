import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
from matplotlib import animation
from numpy import *

ttotal = 20                   # Runs time of 20 seconds
dt = .05                      # time 'resolution' of 0.05 seconds
time = arange(0, ttotal, dt)  # Array of time throughout the simulation

class TennisBall:

    """
        There's a better way of declaring these variables which is by passing them into the construct rather than
        hardcoding their values. I've done it this way for now just so it works, but this can be refactored.
    """
    def __init__(self):
        self.ttotal = 20                                # Runs time of 20 seconds
        self.y0 = 10                                    # Start height of 10m
        self.dt = .05                                   # time 'resolution' of 0.05 seconds
        self.time = arange(0, self.ttotal, self.dt)     # Array of time throughout the simulation
        self.ytemp = zeros(size(self.time))             # Displacement set as zero

        self.gravity = 9.81
        self.cor = 0.83

        self.ax = pl.axes(xlim=(0, self.ttotal), ylim=(0, self.y0))
        self.ax.set_title("Bouncing ball simulation  g = %s  COR = %s" % (self.gravity, self.cor))
        self.line, = self.ax.plot([], [], lw=2)

    """
        Create the base frame for the animation and set it equal to nothing.
        line is the returned object updated for each frame
    """
    def init(self):
        self.line.set_data([], [])
        return self.line,

    """
        Animation function, called sequentially starting at the y-axis
    """
    def animate(self, i):
        # Acquiring data points along the precalculated trajectory of the ball
        t, y_pos = self.ball_height()
        self.ytemp[:i] = y_pos[:i]
        self.line.set_data(t, self.ytemp)
        return self.line,

    """
        Ball_height is the function that calculates the balls hight during its trajectory
    """
    def ball_height(self):
        ydot_0 = 0              # initial velocity
        yddot = -self.gravity   # acceleration due to gravity
        COR = self.cor          # coefficient of restitution for a tennis ball

        y = zeros(size(self.time))
        y0 = self.y0
        y_prev = 0
        cntr = 0

        for idx, t in enumerate(self.time):
            t_local = self.dt * cntr

            y[idx] = y0 + ydot_0 * t_local + yddot * t_local ** 2 / 2

            if y[idx] <= 0.0:
                ydot_0 = COR * (y_prev - y[idx]) / self.dt
                y0 = 0       # must be at edge, reset height to this value
                cntr = 0     # reset counter

            y_prev = y[idx]  # store previous time value
            cntr = cntr + 1  # increment counter

        return self.time, y


"""
    Setup figure and set limits for the graph
"""
fig = pl.figure()

animate = TennisBall()


anim = animation.FuncAnimation(fig, animate.animate, init_func=animate.init, frames=size(time), interval=1, blit=True)

pl.show()

anim.save('bouncingball.mp4', fps=30)

