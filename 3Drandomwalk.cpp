//
// Created by dankemperor on 09/05/18.
//
#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

int main () {
    double dist = 0;
    srand48(time(NULL));                     /// pseudo-random number generator
    ofstream out("xyz_output.txt");          /// output files
    ofstream out2("logarithmic3d.txt");      /// output files
    cout << "x, y and z values for a 3-D random walk" << endl;

    for (int i = 0; i < 101; i++) {
        double x0 = 2 * (2/sqrt(3)) * (drand48() - 0.5);   /// drand - 0.5 to get values between -0.5 and 0.5
        double y0 = 2 * (2/sqrt(3)) * (drand48() - 0.5);   /// drand - 0.5 to get values between -0.5 and 0.5
        double z0 = 2 * (2/sqrt(3)) * (drand48() - 0.5);   /// drand - 0.5 to get values between -0.5 and 0.5
        double x = x + x0;                                 /// update x position
        double y = y + y0;                                 /// update y position
        double z = z + z0;                                 /// update z position
        dist = sqrt(x*x + y*y + z*z);                      /// distance from origin
        cout << "x: " << x << ", y: " << y << ", z: " << z << endl;
        out << x << " " << y << " " << z << endl;
        out2 << log(i) << " " << log(dist) << endl;
    }
    out.close();
}
