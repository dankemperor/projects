from numpy import sin, cos
import numpy as np
import matplotlib.pyplot as pl
import scipy.integrate as integrate
import matplotlib.animation as animation

G = 9.81  # acceleration due to gravity
L1 = 1.0  # length of pendulum 1
L2 = 1.0  # length of pendulum 2
M1 = 1.0  # mass of pendulum 1
M2 = 1.0  # mass of pendulum 2


# The maths behind the double pendulum
def derivs(state, t):
    dydx = np.zeros_like(state)
    dydx[0] = state[1]

    del_ = state[2] - state[0]
    den1 = (M1 + M2)*L1 - M2*L1*cos(del_)*cos(del_)
    dydx[1] = (M2*L1*state[1]*state[1]*sin(del_)*cos(del_) +
               M2*G*sin(state[2])*cos(del_) +
               M2*L2*state[3]*state[3]*sin(del_) -
               (M1 + M2)*G*sin(state[0]))/den1

    dydx[2] = state[3]

    den2 = (L2/L1)*den1
    dydx[3] = (-M2*L2*state[3]*state[3]*sin(del_)*cos(del_) +
               (M1 + M2)*G*sin(state[0])*cos(del_) -
               (M1 + M2)*L1*state[1]*state[1]*sin(del_) -
               (M1 + M2)*G*sin(state[2]))/den2

    return dydx

# Create an array 20 seconds long in 0.05 second intervals
dt = 0.05
t = np.arange(0.0, 20, dt)

# initial values for the simulation
# th1 and th2 are the initial angles
# w1 and w2 are the initial angular velocities
th1 = 210.0
th2 = -30.0
w1 = 0.0
w2 = 0.0

# initial state of the two pendulums
state = np.radians([th1, w1, th2, w2])

# integrate using scipy.integrate module
y = integrate.odeint(derivs, state, t)

# Position of the first pendulum
x1 = L1*sin(y[:, 0])
y1 = -L1*cos(y[:, 0])
# Position of the second pendulum
x2 = L2*sin(y[:, 2]) + x1
y2 = -L2*cos(y[:, 2]) + y1


def init():
    line.set_data([], [])
    time_text.set_text('')
    return line, time_text


# animating the positions of the two pendulums
def animate(i):
    thisx = [0, x1[i], x2[i]]
    thisy = [0, y1[i], y2[i]]

    line.set_data(thisx, thisy)
    time_text.set_text(time_template % (i*dt))
    return line, time_text
    
# Make figure and subplot
fig = pl.figure()
ax = fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
ax.set_aspect('equal')
ax.set_title('Double pendulum')
ax.grid()

line, = ax.plot([], [], 'o-', lw=2)
time_template = 'time = %.1fs'
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

ani = animation.FuncAnimation(fig, animate, np.arange(1, len(y)), interval=25, blit=True, init_func=init)
ani.save('doublependulum.mp4', fps=20)  # command to save the simulation
pl.show()