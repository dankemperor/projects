//
// Created by Barbor on 28/12/17.
//
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cctype>
using namespace std;

int main()  {

    //Setup
    const int MAX_WRONG = 7;            //Maximum number of incorrect guesses allowed.

    vector<string> words;               //Collection of possible words to guess.
    words.push_back("PAPAYA");
    words.push_back("ELIXIR");
    words.push_back("ETHEREAL");

    srand(static_cast<unsigned int>(time(0)));
    random_shuffle(words.begin(), words.end());
    const string THE_WORD = words[0];   //Word to guess.
    int wrong = 0;                      //Number of incorrect guesses.
    string soFar(THE_WORD.size(), '-');  //Word guesses so far
    string used = "";

    cout << "Welcome to the Hangman game. Good luck!\n";

    //Main loop of program
    while ((wrong < MAX_WRONG) && (soFar != THE_WORD))  {
        cout << "\n\nYou have " << (MAX_WRONG - wrong);
        cout << " incorrect guesses left. \n";
        cout << "\nYou have used the following letters: \n" << used << endl;
        cout << "\nSo far, the word is: \n" << soFar << endl;

        char guess;
        cout << "\n\nEnter your guess: ";
        cin >> guess;
        guess = toupper(guess);         //Making both upper case
        while (used.find(guess) != string::npos)    {
            cout << "\nYou have already guessed " << guess << endl;
            cout << "Enter your guess: ";
            cin >> guess;
            guess = toupper(guess);
        }

        used += guess;

        if (THE_WORD.find(guess) != string::npos)   {
            cout << "Congratulations! " << guess << " is in the word. \n";

            //Add the new letter to soFar
            for (int i = 0; i < THE_WORD.length(); ++i) {
                if (THE_WORD[i] == guess)   {
                    soFar[i] = guess;
                }
            }
        }
        else    {
            cout << "Bad luck, "   << guess << " isn't in the word. \n";
            ++wrong;
        }
    }

    //Shutdown/close
    if (wrong == MAX_WRONG) {
        cout << "\nYou've been hanged! better luck next time.";
    }
    else    {
        cout << "\nYou guessed it!";
    }

    cout << "\nThe word was " << THE_WORD << endl;


    return 0;
}
