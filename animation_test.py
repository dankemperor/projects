import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
import matplotlib.animation
from numpy import *

#Simple figure to test outputting an animation

def init():
    global line
    line, = ax.plot(x, zeros_like(x))
    ax.set_xlim(0, 2*pi)
    ax.set_ylim(-1,1)


def animate(i):
    line.set_ydata(sin(2*pi*i / 50)*sin(x))
    return line,

fig = pl.figure()
ax = fig.add_subplot(111)
ax.set_title('Sinusoidal wave')
x = linspace(0, 2*pi, 200)  #Array with values between 0 and 2pi

anim = matplotlib.animation.FuncAnimation(fig, animate, init_func=init, frames=50)
pl.show()
anim.save('testanimation.mp4', fps=30)