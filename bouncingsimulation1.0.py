import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
from matplotlib import animation
from numpy import *


class tennisball:

    def __init__(self, total_time, start_height, dt, time, y_displacement):
        self.total_time = total_time
        self.start_height = start_height
        self.dt = dt
        self.time = time
        self.y_displacement = y_displacement

total_time = 20                    # Runs time of 20 seconds
start_height = 10                  # Start height of 10m
dt = .05                           # time 'resolution' of 0.05 seconds
time = arange(0, total_time, dt)   # Array of time throughout the simulation
y_displacement = zeros(size(time)) # Displacement set as zero


# create the base frame for the animation and set it equal to nothing.
# line is the returned object updated for each frame
def init():
    line.set_data([], [])
    return line,


# animation function, called sequentially starting at the y-axis
def animate(i):
    # Acquiring data points along the precalculated trajectory of the ball
    t, y_pos = ball_height(start_height, total_time, dt)
    y_displacement[:i] = y_pos[:i]
    line.set_data(t, y_displacement)
    return line,


# ball_height is the function that calculates the balls hight during its trajectory
def ball_height(start_height, total_time, dt):
    ydot_0 = 0     # initial velocity
    g = -9.81      # acceleration due to gravity
    COR = .83      # coefficient of restitution for a tennis ball

    time = arange(0, total_time, dt)
    y = zeros(size(time))
    y_displacement = zeros(size(time))
    y[0] = start_height
    y_prev = 0
    t_local = 0
    cntr = 0

    for idx, t in enumerate(time):
        t_local = dt * cntr

        y[idx] = start_height + ydot_0 * t_local + g * t_local ** 2 / 2

        if (y[idx] <= 0.0):
            ydot_0 = COR * (y_prev - y[idx]) / dt
            start_height = 0    # must be at edge, reset height to this value
            t_local = 0         # reset time back to 0
            cntr = 0            # reset counter back to 0

        # calculate position
        y_prev = y[idx]  # store previous time value in array
        cntr = cntr + 1  # increment +1 counter
    return time, y


# setup figure and set limits for the graph
figure = pl.figure()
ax = pl.axes(xlim=(0, total_time), ylim=(0, start_height))
ax.set_title('Bouncing ball simulation  g = 9.81  COR = 0.83')
line, = ax.plot([], [], lw=2)

anim = animation.FuncAnimation(figure, animate, init_func=init,frames=size(time), interval=1, blit=True)
pl.show()
anim.save('bouncingball.mp4', fps=30)

