from random import randrange as rand
from numpy import cos, sin, radians
import numpy as np
import matplotlib.pyplot as pl

N = 100         # Size of square as a multiple of the step size.
NSteps = 5000   # Number of steps in simulation.
x_origin = 0          # x coordinate set for the origin
y_origin = 0          # y coordinate set for the origin

s = 1              # Step number
x = x_origin       # x coordinate of point
y = y_origin       # y coordinate of point
x_positions = []   # List of the x coordinates of all points visited
y_positions = []   # List of the y coordinates of all points visited


def wrap(v, N):
    # The function for tellin when the particle reaches the edge of the box
    if v > N/2:
        return v - N, True
    elif v < -N/2:
        return v + N, True
    return v, False

# Function for random movement in 360 degrees
for j in range(NSteps):
    angle = radians(rand(361))
    x, wrap_flag_x = wrap(x + cos(angle), N)
    y, wrap_flag_y = wrap(y + sin(angle), N)
    if wrap_flag_x or wrap_flag_y:
        x_positions.append(np.nan)
        y_positions.append(np.nan)
    x_positions.append(x)
    y_positions.append(y)

fig, ax = pl.subplots()
ax.plot(x_positions,y_positions,linewidth=0.5)
ax.set_xlim(-N/2,N/2)
ax.set_ylim(-N/2,N/2)
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.set_title('2-D Random Walk')
pl.show()
pl.savefig("2DrandomWalk.png", bbox_inches="tight",dpi = 400)