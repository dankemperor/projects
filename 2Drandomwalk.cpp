//
// Created by Barbor on 09/04/18.
//
#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

int main () {

    double dist = 0;
    srand48(time(NULL));                     /// pseudo-random number generator
    ofstream out("xy_output.txt");           /// output files
    ofstream out2("logarithmic.txt");        /// output files
    cout << "x and y values for a 2-D random walk" << endl;
    for (int i = 1; i < 101; i++) {
        double x0 = 2 * sqrt(2) * (drand48() - 0.5);        /// drand - 0.5 to get values between -0.5 and 0.5
        double y0 = 2 * sqrt(2) * (drand48() - 0.5);        /// drand - 0.5 to get values between -0.5 and 0.5
        double x = x + x0;                                  /// update x position
        double y = y + y0;                                  /// update y position
        dist = sqrt(x*x + y*y);                             /// distance from origin
        cout << "x: " << x << ", " << "y: " << y << endl;
        out << x << " " << y << endl;
        out2 << log(i) << " " << log(dist) << endl;
    }
    out.close();
}
