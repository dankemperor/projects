import numpy as np
import matplotlib.pyplot as pl
import matplotlib.animation as animation
import mpl_toolkits.mplot3d.axes3d as p3

# Using a random seed for reproducibility
np.random.seed(17660406)


# Creating lines using a random-walk algorithm
# length is the number of points for the lines
# dims is the number of dimensions the lines has
def generate_randomline(length, dims=2):
    lineData = np.empty((dims, length))
    lineData[:, 0] = np.random.rand(dims)
    for index in range(1, length):
        # scaling the random numbers by 0.075 so
        # movement is small compared to its position
        # subtraction by 0.5 is to change the range to [-0.5, 0.5]
        # giving the particle a 50/50 chance of moving backwards or forwards
        step = ((np.random.rand(dims) - 0.5) * 0.075)
        lineData[:, index] = lineData[:, index - 1] + step

    return lineData


def update_lines(num,dataLines,lines):
    for line, data in zip(lines, dataLines):
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2, :num])
    return lines

# Fifty lines of random direction in three dimensions
data = [generate_randomline(25, 3) for index in range(50)]

# setting up the figure and 3D axis
fig = pl.figure()
ax = p3.Axes3D(fig)

# Creating fifty line objects
# Three arrays (For x,y,z) filled with zeroes
lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in data]

# Setting the axes limits and labels
ax.set_xlim3d([0.0, 1.0])
ax.set_xlabel('X')
ax.set_ylim3d([0.0, 1.0])
ax.set_ylabel('Y')
ax.set_zlim3d([0.0, 1.0])
ax.set_zlabel('Z')

ax.set_title('Brownian motion of multiple particles in 3D')

# Creating the Animation object
line_ani = animation.FuncAnimation(fig, update_lines, 25, fargs=(data, lines), interval=50, blit=False)
line_ani.save('3Drandomwalk.mp4', fps=20)
pl.show()