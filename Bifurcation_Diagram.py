import math as math
import numpy as np
import matplotlib.pyplot as pl

# Making an array between 0 and 3
r = np.linspace(0, 3, num=1001)

# Make 1001 blank array which are all 1001 long
n = len(r)
a = [0]*len(r)

for i in range(len(r)):
    a[i] = [0]*n
    i += 1

for i in range(len(r)):
    a[i][0] = -0.5

# Compute the the equation with changing r and a values.
# This step is heavy on the computer as you're doing 1001^2 calculations
for j in range(len(r)):
    for k in range(len(a[j])-1):
        a[j][k+1] = r[j]*a[j][k] - (a[j][k])**3
        k += 1
    j += 1

c = np.negative(a)

for i in range(len(a)):
   pl.scatter([r[i]]*502, a[i][499:], alpha=0.2, s=3, edgecolors='none', color='b')
   pl.scatter([r[i]]*502, c[i][499:], alpha=0.2, s=3, edgecolors='none', color='r')

pl.grid()
pl.xlabel("r")
pl.ylabel("x")
pl.xlim(0, 3)
pl.ylim(-2, 2)
pl.show()
